
private void RunScript(Curve C, double x, double y, ref object Raster2DTuple, ref object PixelTuples)
  {
    //written by Pirouz Nourian
    Plane BasePlane = Plane.WorldXY;
    double epsilon = 0.001;
    if((x < epsilon) || (y < epsilon)){throw new Exception("either of the sizes is too tiny to work with!");}
    double tolerance = 0.1 * Math.Min(x, y);
    List<Tuple<int,int>> pixels = new List<Tuple<int,int>>();//could have been called the raster2D as a sparse matrix

    BoundingBox BBox = C.GetBoundingBox(true);
    //B = BBox;

    int iCount = (int) Math.Round(BBox.Diagonal.X / x);
    int jCount = (int) Math.Round(BBox.Diagonal.Y / y);

    Point3d MinCorner = BBox.Min;

    Point3d Origin = new Point3d(MinCorner.X, MinCorner.Y, MinCorner.Z);

    Origin.X = x * Math.Round(MinCorner.X / x);
    Origin.Y = y * Math.Round(MinCorner.Y / y);
    Origin.Z = 0;

    for(int i = 0;i < iCount;i++){
      for(int j = 0;j < jCount;j++){
        Point3d pixelPoint = Origin + i * x * BasePlane.XAxis + j * y * BasePlane.YAxis;
        if(C.Contains(pixelPoint, BasePlane, tolerance) == Rhino.Geometry.PointContainment.Inside){
          Tuple<int,int> pixel = Tuple.Create(i, j);
          pixels.Add(pixel);
        }
      }
    }
    string Description = "pixels as (i,j) tuples, array size as tuple of (int,int), pixel size x,pixelsize y, Plane, origin point";
    Tuple<string,List<Tuple<int,int>>,Tuple<int,int>,double, double, Point3d,Plane> Raster2D;
    Raster2D = Tuple.Create(Description, pixels, Tuple.Create(iCount, jCount), x, y, Origin, BasePlane);
    Raster2DTuple = Raster2D;
    PixelTuples = pixels;
  }
