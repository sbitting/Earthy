﻿##### ##### ##### ##### 
# 0 : Description
##### ##### ##### ##### 

# logical process of the program
# contour the input shape 
# for each contour curve find a list of points that are spaced proportionately to the size of the brick
# based on the curvature of the contour curve find the right orientation of the brick on each point
# create the brick on the plane oriented with the tangent to this point parameter

##### ##### ##### ##### 
# 1 : Initialization: importing libraries and modules that we are going to use
##### ##### ##### ##### 

# Rhino Geometry is a library to handle geometries in Rhino
# Access Documentation: https://developer.rhino3d.com/api/RhinoCommon/html/N_Rhino_Geometry.htm
import Rhino.Geometry as rg 

# Default math module of python to perform basic mathematical calculations
# Access Documentation: https://docs.python.org/3/library/math.html
import math

##### ##### ##### ##### 
# 2 : Inputs: inputs of the program
##### ##### ##### ##### 

# Shape = the brep that we are planning to place bricks on
shp = Shape

# Brick dimensions:
bW = Brick_Width    #X
bD = Brick_Depth    #Y
bH = Brick_Height   #Z

##### ##### ##### ##### 
# 3 : Main Body
##### ##### ##### ##### 

#####
# 3.1 : Contour : in this section we contour the input shape 
#####

# finding the bounding box of the shape
# Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Brep_CreateFromBox.htm
shapeBound = ???

# fining the highest point and lowest point of the shape (max and min of the vertical bound)
# Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Brep_CreateFromBox.htm
maxHeight = shapeBound.Max[???]
minHeight = shapeBound.Min[???]

# contouring the shape by first creating the start and end point
# and storing the contours in countourList
# Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Point3d__ctor_4.htm
# Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Brep_CreateContourCurves_1.htm
sC = ???  # start of contour
eC = ???  # end of contour 
contourList = shp.CreateContourCurves(shp,sC,eC,bH)

#####
# 3.2 : Placement : in this section we place bricks along contours
#####

# we need to initialize two empty lists to save bricks and points
pointList=[]
brickList=[]

# iterating over the contour lines
# Description : https://www.w3schools.com/python/python_for_loops.asp
for curve in contourList:
    # getting the length of the contour curve
    # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Curve_GetLength.htm
    length = ???

    # calculating the diagonal of the brick
    # using Pythagoras' Theorem : https://www.mathopenref.com/rectanglediagonals.html
    brickDiagonal = ???

    # dividing the length of the curve by the diagonal of the brick 
    # to find out how many brick can we place on this line
    # we use math.ceil to make sure that the line is completely covered with bricks
    # Doc : https://docs.python.org/2/library/math.html#math.ceil
    brickNum = ???

    # dividing the curve to find the curve_parameters of the points 
    # that we need to place our bricks on
    # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Curve_DivideByCount.htm
    valueList = curve.DivideByCount(brickNum,True)

    # iterating over the curve_parameters and placing bricks on each point
    # Description : https://www.w3schools.com/python/python_for_loops.asp
    for value in valueList:

        # finding the point in the given value of the curve_parameter
        # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Curve_PointAt.htm
        point = curve.PointAt(value)

        # adding the point to the list of all the points (pointList)
        ???
        
        # defining a frame based on the value of curve_parameter
        # recieve the boolen value and frame
        # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/M_Rhino_Geometry_Curve_FrameAt.htm
        (bool,frame) = curve.FrameAt(value)
        
        # Creating intervals in three dimensions, required to create the brick
        # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/T_Rhino_Geometry_Interval.htm
        xi = rg.Interval(0,bW)                 
        yi = ???
        zi = ???

        # creating a box as the brick with the dimensions specified in intervals (xi, yi,zi)
        # and the plane created based on the direction and position of the curve (frame)
        # Doc : https://developer.rhino3d.com/api/RhinoCommon/html/T_Rhino_Geometry_Box.htm
        brick = ????

        # adding the brick to the list of bricks (brickList)
        ???

##### ##### ##### ##### 
# Outputs: outputs of the program
##### ##### ##### ##### 

# List of the Points that bricks are placed on
Contour_Points = pointList
# List of the Contour Curves
Contour_Curves = contourList
# List of the Bricks
Bricks = brickList